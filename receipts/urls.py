from django.urls import path
from . import views

urlpatterns = [
    path("", views.all_receipts, name="home"),
    path("create/", views.create_receipts, name="create_receipt"),
    path("categories/", views.all_categories, name="category_list"),
    path(
        "categories/create/", views.create_categories, name="create_category"
    ),
    path("accounts/", views.all_accounts, name="account_list"),
    path("accounts/create/", views.create_accounts, name="create_account"),
]
